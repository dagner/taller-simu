import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-fqa',
  templateUrl: './fqa.component.html',
  styleUrls: ['./fqa.component.css']
})
export class FqaComponent implements OnInit {
  qa = {
    idQa: null,
    planPrueba: null,
    herramientaBugTesting: null,
    herramientaBugAutomatizada: null,
    normasCalidad: null
  }
  constructor( public usuariosServicio: UsuariosService) { }

  ngOnInit(): void {
  }

  altaQa() {
    this.usuariosServicio.altaQa(this.qa).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          console.log(this.qa);
        }
      }
    );
  }
}
