import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FdesarrolloComponent } from './fdesarrollo.component';

describe('FdesarrolloComponent', () => {
  let component: FdesarrolloComponent;
  let fixture: ComponentFixture<FdesarrolloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FdesarrolloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FdesarrolloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
