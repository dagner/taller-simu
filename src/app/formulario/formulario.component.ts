import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  usuarios = null;
  respuestasGeneral = null;

  usuario = {
    idUsuario: null,
    nombre: null,
    edad: null,
    genero: null,
    telefono: null,
    email: null,
    departamento: null,
    grado: null,
    areaEspecializacion:null,
    areaEspecializacion2: null,
    implementarSistema: null,
    frecuenciaADesarrollar: null,
    aprenderLenguaje: null,
    frecuenciaDificultad: null,
    pagoTrabajo: null,
    renuncioTrabajo: null,
    tiempoAdaptacion: null,
    tiempoTrabajo: null,
    cursoFormacion: null
  }
  preguntas = {
    id:null,
    pregunta: null
  }
  respuestas = {
    respuesta: null
  }

  constructor( private usuariosServicio: UsuariosService) { }

  
  ngOnInit() {
    this.obtenerUsuarios();
    this.obtenerRespuestas();
  }

  obtenerUsuarios() {
    this.usuariosServicio.obtenerUsuarios()
        .subscribe(
          resultado => this.usuarios = resultado
    );
          console.log(this.usuarios);
  }

  altaUsuario() {
    this.usuariosServicio.altaUsuario(this.usuario).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerUsuarios();
        }
      }
    );
  }

  bajaUsuario(idUsuario) {
    this.usuariosServicio.bajaUsuario(idUsuario).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerUsuarios();
        }
      }
    );
  }

  editarUsuario() {
    this.usuariosServicio.editarUsuario(this.usuario).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerUsuarios();
        }
      }
    );
  }

  seleccionarUsuario(idUsuario) {
    this.usuariosServicio.seleccionarUsuario(idUsuario).subscribe(
      result => this.usuario = result[0]
    );
  }

  hayRegistros() {
    if(this.usuarios == null) {
      return false;
    } else {
      return true;
    }
  }
  clasificarUsuarios(){
    this.usuariosServicio.clasificarUsuario()
    .subscribe(
      result => this.usuarios = result[0].Count
    );
    console.log(this.usuarios);
    console.log();
    // console.log(this.usuarios[0].Count); //suma de todos los q son de Cocha
    // this.usuarios[0] = this.sumaUsuarios;
    // console.log(this.sumaUsuarios);
  }
  

  altaPregunta() {
    this.usuariosServicio.altaPreguntas(this.preguntas).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          // this.obtenerUsuarios();
          console.log(this.preguntas);
          console.log(this.preguntas.id);
        }
      }
      // console.log(this.pregunta);
    );
  }
  altaRespuesta() {
    this.usuariosServicio.altaRespuestas(this.respuestas).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          // this.obtenerUsuarios();
          console.log(this.respuestas);
        }
      }
      // console.log(this.pregunta);
    );
  }

  obtenerRespuestas() {
    this.usuariosServicio.obtenerRespuestas()
        .subscribe(
          datos => this.respuestasGeneral = datos
    );
          console.log(this.respuestasGeneral);
  }
}
