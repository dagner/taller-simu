import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FredesComponent } from './fredes.component';

describe('FredesComponent', () => {
  let component: FredesComponent;
  let fixture: ComponentFixture<FredesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FredesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FredesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
