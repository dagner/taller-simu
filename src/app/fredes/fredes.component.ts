import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-fredes',
  templateUrl: './fredes.component.html',
  styleUrls: ['./fredes.component.css']
})
export class FredesComponent implements OnInit {
  redes = {
    idRedes: null,
    experienciaRedes: null,
    protocolosYServicios: null,
    webServerCisco: null,
    conocimientoSistemas: null
  }

  constructor( public usuariosServicio: UsuariosService) { }

  ngOnInit(): void {
  }

  altaRedes() {
    this.usuariosServicio.altaRedes(this.redes).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          console.log(this.redes);
        }
      }
    );
  }
}
