import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FempresaComponent } from './fempresa.component';

describe('FempresaComponent', () => {
  let component: FempresaComponent;
  let fixture: ComponentFixture<FempresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FempresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FempresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
