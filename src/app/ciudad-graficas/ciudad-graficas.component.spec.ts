import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CiudadGraficasComponent } from './ciudad-graficas.component';

describe('CiudadGraficasComponent', () => {
  let component: CiudadGraficasComponent;
  let fixture: ComponentFixture<CiudadGraficasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CiudadGraficasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CiudadGraficasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
