import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaGraficasComponent } from './lista-graficas.component';

describe('ListaGraficasComponent', () => {
  let component: ListaGraficasComponent;
  let fixture: ComponentFixture<ListaGraficasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaGraficasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaGraficasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
