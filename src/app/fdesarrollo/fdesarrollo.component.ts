import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-fdesarrollo',
  templateUrl: './fdesarrollo.component.html',
  styleUrls: ['./fdesarrollo.component.css']
})
export class FdesarrolloComponent implements OnInit {
    desarrollo = {
      idDesarrollo: null,
      numLenguaje: null,
      lenguaje: null,
      numEmpresas: null,
      controladorVersion: null
    }
  constructor( public usuariosServicio : UsuariosService) { }

  ngOnInit(): void {
  }
  altaDesarrollo() {
    this.usuariosServicio.altaDesarrollo(this.desarrollo).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          console.log(this.desarrollo);
        }
      }
    );
  }
  

}
