import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { DatosComponent } from './pages/datos/datos.component';
import { GraficasComponent } from './pages/graficas/graficas.component';
import { RemoveCommaPipe } from './pipes/comma.pipe';

import { ChartsModule } from 'ng2-charts';
import { HomeComponent } from './pages/home/home.component';
import { CuarentenaComponent } from './pages/cuarentena/cuarentena.component';

import { MatDatepickerModule } from '@angular/material/datepicker';
import {  MatNativeDateModule, MatOptionSelectionChange, MatPseudoCheckbox, MatPseudoCheckboxModule, MAT_DATE_LOCALE } from '@angular/material/core';

import { PdfMakeWrapper } from 'pdfmake-wrapper';
import pdfFonts from "pdfmake/build/vfs_fonts";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormularioComponent } from './formulario/formulario.component'; // fonts provided for pdfmake
//servicio
import { UsuariosService } from './usuarios.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ListaComponent } from './lista/lista.component';
import { MatRadioModule } from '@angular/material/radio';
import { ContentComponent } from './content/content.component';

//angular material
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatGridListModule} from '@angular/material/grid-list';
import { PanelComponent } from './panel/panel.component';
import { FempresaComponent } from './fempresa/fempresa.component';
import { FbasededatosComponent } from './fbasededatos/fbasededatos.component';
import { FdesarrolloComponent } from './fdesarrollo/fdesarrollo.component';
import { FqaComponent } from './fqa/fqa.component';
import { FredesComponent } from './fredes/fredes.component';
import { FhelpDeskComponent } from './fhelp-desk/fhelp-desk.component';
import { ListaGraficasComponent } from './lista-graficas/lista-graficas.component';
import { CiudadGraficasComponent } from './ciudad-graficas/ciudad-graficas.component';

// Set the fonts to use
PdfMakeWrapper.setFonts(pdfFonts);
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    SidebarComponent,
    DatosComponent,
    GraficasComponent,
    RemoveCommaPipe,
    HomeComponent,
    CuarentenaComponent,
    FormularioComponent,
    ListaComponent,
    ContentComponent,
    PanelComponent,
    FempresaComponent,
    FbasededatosComponent,
    FdesarrolloComponent,
    FqaComponent,
    FredesComponent,
    FhelpDeskComponent,
    ListaGraficasComponent,
    CiudadGraficasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    HttpClientModule,
    MatRadioModule,
    MatToolbarModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatSelectModule,
    MatGridListModule,
  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule,
    UsuariosService,
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
