import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FhelpDeskComponent } from './fhelp-desk.component';

describe('FhelpDeskComponent', () => {
  let component: FhelpDeskComponent;
  let fixture: ComponentFixture<FhelpDeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FhelpDeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FhelpDeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
