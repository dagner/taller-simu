import { Component, OnInit, NgZone } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { toNumberOrPercent } from '@amcharts/amcharts4/.internal/core/utils/Type';

@Component({
  selector: 'app-ciudad-graficas',
  templateUrl: './ciudad-graficas.component.html',
  styleUrls: ['./ciudad-graficas.component.css']
})
export class CiudadGraficasComponent implements OnInit {

  chart: am4charts.XYChart;

  constructor( private usuariosServicio: UsuariosService,
               private zone: NgZone ) {
  am4core.useTheme(am4themes_animated);
  }
  
  usuarios = 33;
  usuariosLPZ = 21;
  usuariosSCZ = 14;


  ngOnInit(): void {
    
    var chart = am4core.create("chartdiv2", am4charts.PieChart);
    
    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";
    
    // Let's cut a hole in our Pie chart the size of 30% the radius
    chart.innerRadius = am4core.percent(5);
    pieSeries.colors.list = [
      am4core.color('#5392ff'),
      am4core.color('#95d13c'),
      am4core.color('#fe8500'),
      am4core.color('#FFD65A'),
      am4core.color('#8FF58C'),
      am4core.color('#00D4F4'),
      am4core.color('#9b82f3'),
      am4core.color('#34bc6e'),
      am4core.color('#FC585C'),
      am4core.color('#00E9C1'),
      am4core.color('#FFB15A'),
      am4core.color('#00CBDF')
    ];
    // Put a thick white border around each Slice
    // pieSeries.slices.template
    //   // change the cursor on hover to make it apparent the object can be interacted with
    //   .cursorOverStyle = [
      //     {
        //       "property": "cursor",
        //       "value": "pointer"
        //     }
        //   ];
        
        // pieSeries.alignLabels = false;
        // pieSeries.labels.template.bent = true;
        // pieSeries.labels.template.radius = 3;
        // pieSeries.labels.template.padding(0,0,0,0);
        pieSeries.ticks.template.disabled = true;
        pieSeries.labels.template.hidden = true;
        pieSeries.tooltip.disabled = true;
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        // Create a base filter effect (as if it's not there) for the hover to return to
        // var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        // shadow.opacity = 0;
        
        // Create hover state
        // var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists
        
        // Slightly shift the shadow and make it more prominent on hover
        // var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        // hoverShadow.opacity = 0.7;
        // hoverShadow.blur = 5;
        
        // Add a legend
        chart.legend = new am4charts.Legend();
        
        chart.data = [
          {
          "country": "Cochabamba",
          "litres": this.usuarios
        },{
          "country": "La Paz",
          "litres": this.usuariosLPZ
        },{
          "country": "Santa Cruz",
          "litres": this.usuariosSCZ
        }
      ];
        
      

  }
  clasificarUsuariosCBBA2(){
    this.usuariosServicio.clasificarUsuario()
    .subscribe(
      result => this.usuarios = Number(result[0].Count)
    );
    console.log(typeof(this.usuarios));
  }
  clasificarUsuariosLPZ2(){
    this.usuariosServicio.clasificarUsuarioLPZ()
    .subscribe(
      result => this.usuariosLPZ = parseInt(result[0].Count)
    );
    console.log(typeof(this.usuariosLPZ));
  }
  clasificarUsuariosSCZ2(){
    this.usuariosServicio.clasificarUsuarioSCZ()
    .subscribe(
      result => this.usuariosSCZ = parseInt(result[0].Count)
    );
    console.log(typeof(this.usuariosSCZ));
  }

}
