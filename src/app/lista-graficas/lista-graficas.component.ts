import { Component, NgZone, OnInit, ViewChild, ElementRef} from '@angular/core';
import { UsuariosService } from '../usuarios.service';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import { PdfMakeWrapper, Txt, Columns, Stack, Table, TextReference, PageReference, Img } from 'pdfmake-wrapper';
import * as jsPDF from "jspdf";
import { variable } from '@angular/compiler/src/output/output_ast';
import { element } from 'protractor';

 var $: any;
@Component({
  selector: 'app-lista-graficas',
  templateUrl: './lista-graficas.component.html',
  styleUrls: ['./lista-graficas.component.css']
})
export class ListaGraficasComponent implements OnInit {
  @ViewChild('content') content : ElementRef ;
  name = 'Angular';
  chart: am4charts.XYChart;

  genero: number[] = [];
  // listaSusceptible: number[] = []
  femenino: number[]=[];
  hom = 28;
  fem = 19;
  hombres;
  mujeres;
  fecha;
  dato=0;
 

  constructor(private zone: NgZone,
              private usuariosServicio: UsuariosService) {
    am4core.useTheme(am4themes_animated);
  }
  
  
  ngOnInit() {
    this.fecha = new Date().getDate() + '/' + new Date().getMonth()+1 + '/' + new Date().getFullYear();
    var chart = am4core.create("chartdiv", am4charts.PieChart);
    
    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";
    
    // Let's cut a hole in our Pie chart the size of 30% the radius
    chart.innerRadius = am4core.percent(5);
    pieSeries.colors.list = [
      am4core.color('#5392ff'),
      am4core.color('#95d13c'),
      am4core.color('#fe8500'),
      am4core.color('#FFD65A'),
      am4core.color('#8FF58C'),
      am4core.color('#00D4F4'),
      am4core.color('#9b82f3'),
      am4core.color('#34bc6e'),
      am4core.color('#FC585C'),
      am4core.color('#00E9C1'),
      am4core.color('#FFB15A'),
      am4core.color('#00CBDF')
    ];
    // Put a thick white border around each Slice
    // pieSeries.slices.template
    //   // change the cursor on hover to make it apparent the object can be interacted with
    //   .cursorOverStyle = [
      //     {
        //       "property": "cursor",
        //       "value": "pointer"
        //     }
        //   ];
        
        // pieSeries.alignLabels = false;
        // pieSeries.labels.template.bent = true;
        // pieSeries.labels.template.radius = 3;
        // pieSeries.labels.template.padding(0,0,0,0);
        pieSeries.ticks.template.disabled = true;
        pieSeries.labels.template.hidden = true;
        pieSeries.tooltip.disabled = true;
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        // Create a base filter effect (as if it's not there) for the hover to return to
        // var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        // shadow.opacity = 0;
        
        // Create hover state
        // var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists
        
        // Slightly shift the shadow and make it more prominent on hover
        // var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        // hoverShadow.opacity = 0.7;
        // hoverShadow.blur = 5;
        
        // Add a legend
        chart.legend = new am4charts.Legend();
        
        chart.data = [{
          "country": "Hombres",
          "litres": this.hom
          // (parseInt($('#probContagios').val()))
        }, {
          "country": "Mujeres",
          "litres": this.fem
        }];
        
      }
      
      porGeneroGra(){
        this.usuariosServicio.porGeneroGra()
        .subscribe(
          result => this.genero = result[0]
          );
          console.log(this.genero);
        }
        porGeneroFemeninoGra(){
          this.usuariosServicio.porGeneroFemeninoGra()
          .subscribe(
            result => this.femenino = (result[0])
            );
            console.log(this.femenino);
          }

          porMasculino(){
            this.usuariosServicio.porGenero()
            .subscribe(
              result => this.hombres = result
            );
            console.log(this.hombres);
          }
          porFemenino(){
            this.usuariosServicio.porGeneroFemenino()
            .subscribe(
              result => this.mujeres = result
            );
            console.log(this.mujeres);
          }
          for(){
            for (var v=0; v<this.hombres.length; v++) { 
              this.dato++; }
          }
          
        
        
          
          
          // ngOnDestroy() {
            //   this.zone.runOutsideAngular(() => {
              //     if (this.chart) {
                //       this.chart.dispose();
                //     }
                //   });
                // }
                
                
                
                
                async pdf(){
                  var canvas = $("#chart").get(0);
    var dataURL = canvas.toDataURL();
    //console.log(dataURL);
    var pdf = new PdfMakeWrapper();
    pdf.images({
      picture1: await new Img(dataURL).build()
    })
    pdf.create().open();
    
  }
  
  //Funcion PDF por genero
  pdfGenero() {
    var pdf = new PdfMakeWrapper();
    pdf.pageMargins([50, 60]);
    pdf.info({
      title: 'reporte',
      author: 'Taller de Simulacion'
    });
    //marca de agua
    pdf.watermark({ text: 'Clasificacion por Genero', color: '#f2f2f2', opacity: 0.3, bold: true });
    
    //la cabecera - el titulo
    pdf.add(new Txt('Reporte: Clasificación por Genero').alignment('center').fontSize(20).color('#191970').bold().end);
    pdf.add(
      pdf.ln(1)
      );
      //el pie de pagina
      pdf.footer(new Txt('- Documento sin validez legal').fontSize(15).color('#f2f2f2').opacity(0.5).alignment('center').end);
      pdf.add(new Txt('Fecha: ' + this.fecha).fontSize(12).color('#191970').bold().alignment('right').end);
      pdf.add(
        pdf.ln(1)
        );
        pdf.add(new Txt('> Profesionales Registrados').fontSize(20).color('#008b8b').bold().end);
        pdf.add(
          pdf.ln(1)
          );
          pdf.add(
            new Table([
              [{ text: this.capitalize('Profesionales Hombres'), fontSize: 18, bold: true },
              { text: this.hombres.length, fontSize: 15, bold: false, alignment: 'right' }],
              
              [{ text: this.capitalize('Profesionales Mujeres'), fontSize: 18, bold: true },
              { text: this.mujeres.length, fontSize: 15, bold: false, alignment: 'right' }],
            ]).widths([ 300, '*' ]).end
            );
            
            //salto de lineas
            pdf.add(
              pdf.ln(1)
              );
              pdf.add(new Txt('Recientes').fontSize(20).color('#008b8b').bold().alignment("center").end);
              pdf.add(
                
                new Table([
                  [{ text: this.capitalize('Hombres'), fontSize: 17, bold: true },
                  { text: this.capitalize('Mujeres'), fontSize: 17, bold: true, }],
                    
                  [{ text: this.hombres[28].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[17].nombre, fontSize: 14, bold: false}],
                  [{ text: this.hombres[27].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[16].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[26].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[15].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[25].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[14].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[24].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[13].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[23].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[12].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[22].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[11].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[21].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[10].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[20].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[9].nombre, fontSize: 14, bold: false }],
                  [{ text: this.hombres[19].nombre, fontSize: 14, bold: false },
                  { text: this.mujeres[8].nombre, fontSize: 14, bold: false }],
                ]).widths([ 240, 240 ]).alignment("center").end
                );
              // para la fecha ('+this.cantDias+' dias'+'):
              pdf.add(
                pdf.ln(1)
                );
                
                
                
                
                pdf.defaultStyle({
                  bold: true,
                  fontSize: 20
                });
                pdf.create().open();
              }
              capitalize(word) {
                return word[0].toUpperCase() + word.slice(1);
              }
              
              
              
              
              public downloadPDF(){
                let doc = new jsPDF();
                let specialElementHandlers = {
      '#editor': function(element , renderer){
        return true ; 
      }
    };
    let content = this.content.nativeElement;
    doc.fromHTML(content.innerHTML,15,15,{

      'width': 190,
      'elementHandlers':specialElementHandlers
    });

  doc.save('test.pdf');
  }


}
