import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-fhelp-desk',
  templateUrl: './fhelp-desk.component.html',
  styleUrls: ['./fhelp-desk.component.css']
})
export class FhelpDeskComponent implements OnInit {
  helpdesk = {
    idHelpDesk : null,
    habilidadDeComunicacion: null,
    capacidadDeSolucion: null,
    explicarInformacion: null,
    analizarMultiplesTareas: null
  }

  constructor( public usuariosServicio: UsuariosService) { }

  ngOnInit(): void {
  }

  altaHelpDesk() {
    this.usuariosServicio.altaHelpDesk(this.helpdesk).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          console.log(this.helpdesk);
        }
      }
    );
  }
}
