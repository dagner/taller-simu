import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatosGraficaService {

  listaSusceptible = [];
  listaInfectados = [];
  listaRecuperados = [];
  listaFallecidos = [];
  listaContagios = [];
  listaRecuperaciones = [];
  listaFallecimientos = [];
  dias = [];

  constructor() { }
}
