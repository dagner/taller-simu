import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-fempresa',
  templateUrl: './fempresa.component.html',
  styleUrls: ['./fempresa.component.css']
})
export class FempresaComponent implements OnInit {
  empresa = {
    idEmpresa: null,
    nombreEmpresa: null,
    numTrabajadores:null,
    requisitoEmpleado: null,
    usoSoftware: null,
    sistemaOperativo: null,
    tiempoOperacion: null,
    usoLenguajes: null,
    usoMetodologias: null
  }
  constructor( public usuariosServicio: UsuariosService) { }

  ngOnInit(): void {
  }

  altaEmpresa() {
    this.usuariosServicio.altaEmpresa(this.empresa).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          console.log(this.empresa);
        }
      }
    );
  }
}
