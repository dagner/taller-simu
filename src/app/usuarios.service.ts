import { Injectable, NgModule } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  URL = "http://localhost/api/";

  constructor(private http: HttpClient) { }

  obtenerUsuarios() {
    return this.http.get(`${this.URL}ObtenerUsuarios.php`);
  }

  altaUsuario(usuario) {
    return this.http.post(`${this.URL}AltaUsuario.php`, JSON.stringify(usuario));
  }

  bajaUsuario(idUsuario: number) {
    return this.http.get(`${this.URL}BajaUsuario.php?idUsuario=${idUsuario}`);
  }

  seleccionarUsuario(idUsuario: number) {
    return this.http.get(`${this.URL}SeleccionarUsuario.php?idUsuario=${idUsuario}`);
  }

  editarUsuario(usuario) {
    return this.http.post(`${this.URL}EditarUsuario.php`, JSON.stringify(usuario));
  }
  clasificarUsuarioLPZ(){
    return this.http.get(`${this.URL}ClasificarUsuarioLPZ.php`);
  }
  clasificarUsuarioSCZ(){
    return this.http.get(`${this.URL}ClasificarUsuarioSCZ.php`);
  }
  clasificarUsuario(){
    return this.http.get(`${this.URL}ClasificarUsuario.php`);
  }
  altaPreguntas(pregunta){
    return this.http.post(`${this.URL}AltaPreguntas.php`, JSON.stringify(pregunta));
  }
  altaRespuestas(respuesta){
    return this.http.post(`${this.URL}AltaRespuestas.php`, JSON.stringify(respuesta));
  }
  obtenerRespuestas() {
    return this.http.get(`${this.URL}ObtenerRespuestas.php`);
  }
  clasificarGrado(){
    return this.http.get(`${this.URL}ClasificarGrado.php`);
  }
  clasificarGrado2(){
    return this.http.get(`${this.URL}ClasificarGrado2.php`);
  }
  porGenero(){
    return this.http.get(`${this.URL}PorGenero.php`);
  }
  porGeneroFemenino(){
    return this.http.get(`${this.URL}PorGeneroFemenino.php`);
  }
  porGeneroGra(){
    return this.http.get(`${this.URL}PorGeneroGra.php`);
  }
  porGeneroFemeninoGra(){
    return this.http.get(`${this.URL}PorGeneroFemeninoGra.php`);
  }
  altaBd(baseDato){
    return this.http.post(`${this.URL}AltaBd.php`, JSON.stringify(baseDato));
  }
  altaEmpresa(empresa){
    return this.http.post(`${this.URL}AltaEmpresa.php`, JSON.stringify(empresa));
  }
  altaDesarrollo(desarrollo){
    return this.http.post(`${this.URL}AltaDesarrollo.php`, JSON.stringify(desarrollo));
  }
  altaRedes(redes){
    return this.http.post(`${this.URL}AltaRedes.php`, JSON.stringify(redes));
  }
  altaQa(qa){
    return this.http.post(`${this.URL}AltaQa.php`, JSON.stringify(qa));
  }
  altaHelpDesk(helpdesk){
    return this.http.post(`${this.URL}AltaHelpDesk.php`, JSON.stringify(helpdesk));
  }
  clasificarDev1(){
    return this.http.get(`${this.URL}ClasificarDev1.php`);
  }
  clasificarDev3(){
    return this.http.get(`${this.URL}ClasificarDev3.php`);
  }
  clasificarDev5(){
    return this.http.get(`${this.URL}ClasificarDev5.php`);
  }
  clasificarBd1(){
    return this.http.get(`${this.URL}ClasificarBd1.php`);
  }
  clasificarBd3(){
    return this.http.get(`${this.URL}ClasificarBd3.php`);
  }
  clasificarBd5(){
    return this.http.get(`${this.URL}ClasificarBd5.php`);
  }
  clasificarHd1(){
    return this.http.get(`${this.URL}ClasificarHd1.php`);
  }
  clasificarHd3(){
    return this.http.get(`${this.URL}ClasificarHd3.php`);
  }
  clasificarHd5(){
    return this.http.get(`${this.URL}ClasificarHd5.php`);
  }
  clasificarRedes1(){
    return this.http.get(`${this.URL}ClasificarRedes1.php`);
  }
  clasificarRedes3(){
    return this.http.get(`${this.URL}ClasificarRedes3.php`);
  }
  clasificarRedes5(){
    return this.http.get(`${this.URL}ClasificarRedes5.php`);
  }
}
