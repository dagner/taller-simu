import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-fbasededatos',
  templateUrl: './fbasededatos.component.html',
  styleUrls: ['./fbasededatos.component.css']
})
export class FbasededatosComponent implements OnInit {

    baseDato ={
      idBd: null,
      gestorRelacional: null,
      gestorNoRelacional: null,
      experienciaBd: null,
      problemasTrabajo: null
    }
  constructor( public usuariosServicio: UsuariosService) { }

  ngOnInit(): void {
  }

  altaBd() {
    this.usuariosServicio.altaBd(this.baseDato).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          console.log(this.baseDato);
        }
      }
    );
  }

}
