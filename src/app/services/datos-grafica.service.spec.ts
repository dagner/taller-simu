import { TestBed } from '@angular/core/testing';

import { DatosGraficaService } from './datos-grafica.service';

describe('DatosGraficaService', () => {
  let service: DatosGraficaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatosGraficaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
