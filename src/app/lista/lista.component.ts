import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';
@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  usuarios = null;
  usuariosLPZ = null;
  usuariosSCZ = null;
  masculino = null;
  femenino = null;
  genero = null;
  gradoAcademico = null;

  usuario = {
    idUsuario: null,
    nombre: null,
    edad: null,
    genero: null,
    telefono: null,
    email: null,
    departamento: null,
    grado: null,
    areaEspecializacion:null,
    areaEspecializacion2: null,
    implementarSistema: null,
    frecuenciaADesarrollar: null,
    aprenderLenguaje: null,
    frecuenciaDificultad: null,
    pagoTrabajo: null,
    renuncioTrabajo: null,
    tiempoAdaptacion: null,
    tiempoTrabajo: null,
    cursoFormacion: null
  }

  constructor( private usuariosServicio: UsuariosService) { }

  ngOnInit(): void {
    this.obtenerUsuarios();
  }
  obtenerUsuarios() {
    this.usuariosServicio.obtenerUsuarios()
        .subscribe(
          result => this.usuarios = result
    );
          console.log(this.usuarios);
  }

  altaUsuario() {
    this.usuariosServicio.altaUsuario(this.usuario).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerUsuarios();
        }
      }
    );
  }

  bajaUsuario(idUsuario) {
    this.usuariosServicio.bajaUsuario(idUsuario).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerUsuarios();
        }
      }
    );
  }

  editarUsuario() {
    this.usuariosServicio.editarUsuario(this.usuario).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerUsuarios();
        }
      }
    );
  }

  seleccionarUsuario(idUsuario) {
    this.usuariosServicio.seleccionarUsuario(idUsuario).subscribe(
      result => this.usuario = result[0]
    );
  }

  hayRegistros() {
    if(this.usuarios == null) {
      return false;
    } else {
      return true;
    }
  }
  clasificarUsuariosCBBA(){
    this.usuariosServicio.clasificarUsuario()
    .subscribe(
      result => this.usuarios = result[0].Count
    );
    console.log(this.usuarios);
  }
  clasificarUsuariosLPZ(){
    this.usuariosServicio.clasificarUsuarioLPZ()
    .subscribe(
      result => this.usuariosLPZ = result[0].Count
    );
    console.log(this.usuariosLPZ);
  }
  clasificarUsuariosSCZ(){
    this.usuariosServicio.clasificarUsuarioSCZ()
    .subscribe(
      result => this.usuariosSCZ = result[0].Count
    );
    console.log(this.usuariosSCZ);
  }

  clasificarGradoAcademico(){
    this.usuariosServicio.clasificarGrado()
    .subscribe(
      result => this.usuarios = result
    );
    console.log(this.usuarios);
  }
  porGenero(){
    this.usuariosServicio.porGenero()
    .subscribe(
      result => this.genero = result
    );
    console.log(this.genero);
  }
  porGeneroFemenino(){
    this.usuariosServicio.porGeneroFemenino()
    .subscribe(
      result => this.femenino = result
    );
    console.log(this.femenino);
  }

  clasificarGradoAcademico2(){
    this.usuariosServicio.clasificarGrado2()
    .subscribe(
      result => this.usuarios = result
    );
    console.log(this.usuarios);
  }

  clasificarDev1(){
    this.usuariosServicio.clasificarDev1()
    .subscribe(
      result => this.usuarios = result
    );
  }
  clasificarDev3(){
    this.usuariosServicio.clasificarDev3()
    .subscribe(
      result => this.usuarios = result
    );
  }

  clasificarDev5(){
    this.usuariosServicio.clasificarDev5()
    .subscribe(
      result => this.usuarios = result
    );
    console.log(this.usuarios);
  }

  clasificarBd1(){
    this.usuariosServicio.clasificarBd1()
    .subscribe(
      result => this.usuarios = result
    );
  }
  clasificarBd3(){
    this.usuariosServicio.clasificarBd3()
    .subscribe(
      result => this.usuarios = result
    );
  }

  clasificarBd5(){
    this.usuariosServicio.clasificarBd5()
    .subscribe(
      result => this.usuarios = result
    );
    console.log(this.usuarios);
  }
  clasificarHd1(){
    this.usuariosServicio.clasificarHd1()
    .subscribe(
      result => this.usuarios = result
    );
  }
  clasificarHd3(){
    this.usuariosServicio.clasificarHd3()
    .subscribe(
      result => this.usuarios = result
    );
  }

  clasificarHd5(){
    this.usuariosServicio.clasificarHd5()
    .subscribe(
      result => this.usuarios = result
    );
    console.log(this.usuarios);
  }

  clasificarRedes1(){
    this.usuariosServicio.clasificarRedes1()
    .subscribe(
      result => this.usuarios = result
    );
  }
  clasificarRedes3(){
    this.usuariosServicio.clasificarRedes3()
    .subscribe(
      result => this.usuarios = result
    );
  }

  clasificarRedes5(){
    this.usuariosServicio.clasificarRedes5()
    .subscribe(
      result => this.usuarios = result
    );
    console.log(this.usuarios);
  }
}
