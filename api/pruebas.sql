-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-12-2020 a las 16:33:31
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bd`
--

CREATE TABLE `bd` (
  `idBd` int(10) NOT NULL,
  `gestorRelacional` varchar(100) NOT NULL,
  `gestorNoRelacional` varchar(100) NOT NULL,
  `experienciaBd` varchar(100) NOT NULL,
  `problemasTrabajo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bd`
--

INSERT INTO `bd` (`idBd`, `gestorRelacional`, `gestorNoRelacional`, `experienciaBd`, `problemasTrabajo`) VALUES
(1, 'MySQL', 'Cassandra', 'Si', 'No'),
(2, 'PostgresSQL', 'DynamoDB', 'Si', 'No'),
(3, 'Oracle', 'MongoDB', 'Si', 'No');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrollo`
--

CREATE TABLE `desarrollo` (
  `idDesarrollo` int(11) NOT NULL,
  `numLenguaje` int(11) NOT NULL,
  `lenguaje` varchar(100) NOT NULL,
  `numEmpresas` varchar(100) NOT NULL,
  `controladorVersion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `desarrollo`
--

INSERT INTO `desarrollo` (`idDesarrollo`, `numLenguaje`, `lenguaje`, `numEmpresas`, `controladorVersion`) VALUES
(1, 2, 'Python', '', 'git'),
(2, 2, 'otro', '1', 'git'),
(3, 0, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `idEmpresa` int(11) NOT NULL,
  `nombreEmpresa` varchar(100) NOT NULL,
  `numTrabajadores` int(11) NOT NULL,
  `requisitoEmpleado` varchar(100) NOT NULL,
  `usoSoftware` varchar(100) NOT NULL,
  `sistemaOperativo` varchar(100) NOT NULL,
  `tiempoOperacion` int(11) NOT NULL,
  `usoLenguajes` varchar(100) NOT NULL,
  `usoMetodologias` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`idEmpresa`, `nombreEmpresa`, `numTrabajadores`, `requisitoEmpleado`, `usoSoftware`, `sistemaOperativo`, `tiempoOperacion`, `usoLenguajes`, `usoMetodologias`) VALUES
(1, 'DevSoft', 25, 'ci, passport, licence', 'libre', 'linux', 2, 'javascript', 'scrum');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `helpdesk`
--

CREATE TABLE `helpdesk` (
  `idHelpDesk` int(11) NOT NULL,
  `habilidadDeComunicacion` varchar(100) NOT NULL,
  `capacidadDeSolucion` varchar(100) NOT NULL,
  `explicarInformacion` varchar(100) NOT NULL,
  `analizarMultiplesTareas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `helpdesk`
--

INSERT INTO `helpdesk` (`idHelpDesk`, `habilidadDeComunicacion`, `capacidadDeSolucion`, `explicarInformacion`, `analizarMultiplesTareas`) VALUES
(1, 'Si', 'No', '', 'Si'),
(2, 'Si', 'No', 'No', 'Si'),
(3, 'Si', 'Si', 'No', 'Si'),
(4, 'Si', 'No', 'No', 'Si'),
(5, 'Si', 'No', 'No', 'Si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntarespuesta`
--

CREATE TABLE `preguntarespuesta` (
  `id` int(11) NOT NULL,
  `idPregunta` int(3) NOT NULL,
  `idRespuesta` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `preguntarespuesta`
--

INSERT INTO `preguntarespuesta` (`id`, `idPregunta`, `idRespuesta`) VALUES
(1, 1, 1),
(2, 1, 4),
(3, 17, 5),
(4, 17, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `id` int(11) NOT NULL,
  `pregunta` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id`, `pregunta`) VALUES
(1, '¿cuantos años tienes?'),
(2, ''),
(8, ''),
(9, ''),
(10, 'ok?'),
(11, 'cuantos aÃ±os tienes?'),
(12, 'cuantos aÃ±os tienes?'),
(13, 'how'),
(14, 'how are you'),
(15, 'Grado academico'),
(16, 'Grado acadÃ©mico'),
(17, 'Grado academico'),
(18, 'grado'),
(19, 'grado de especialidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qa`
--

CREATE TABLE `qa` (
  `idQa` int(11) NOT NULL,
  `planPrueba` varchar(100) NOT NULL,
  `herramientaBugTesting` varchar(100) NOT NULL,
  `herramientaBugAutomatizada` varchar(100) NOT NULL,
  `normasCalidad` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qa`
--

INSERT INTO `qa` (`idQa`, `planPrueba`, `herramientaBugTesting`, `herramientaBugAutomatizada`, `normasCalidad`) VALUES
(2, 'Si', 'Tracker', 'Selenium', 'Si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes`
--

CREATE TABLE `redes` (
  `idRedes` int(11) NOT NULL,
  `experienciaRedes` varchar(100) NOT NULL,
  `protocolosYServicios` varchar(100) NOT NULL,
  `webServerCisco` varchar(100) NOT NULL,
  `conocimientoSistemas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `redes`
--

INSERT INTO `redes` (`idRedes`, `experienciaRedes`, `protocolosYServicios`, `webServerCisco`, `conocimientoSistemas`) VALUES
(1, 'Si', 'Si', 'No', 'Si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `idRespuesta` int(100) NOT NULL,
  `respuesta` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`idRespuesta`, `respuesta`) VALUES
(1, '18'),
(2, 'si'),
(3, 'no'),
(4, '30'),
(5, 'Bachiller'),
(6, 'bachiller'),
(7, 'tecnico'),
(8, ''),
(9, 'diplomado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `edad` int(100) NOT NULL,
  `genero` varchar(30) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `departamento` text NOT NULL,
  `grado` text NOT NULL,
  `areaEspecializacion` text NOT NULL,
  `areaEspecializacion2` text NOT NULL,
  `implementarSistema` text NOT NULL,
  `frecuenciaADesarrollar` text NOT NULL,
  `aprenderLenguaje` text NOT NULL,
  `frecuenciaDificultad` text NOT NULL,
  `pagoTrabajo` text NOT NULL,
  `renuncioTrabajo` text NOT NULL,
  `tiempoAdaptacion` text NOT NULL,
  `tiempoTrabajo` text NOT NULL,
  `cursoFormacion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `edad`, `genero`, `telefono`, `email`, `departamento`, `grado`, `areaEspecializacion`, `areaEspecializacion2`, `implementarSistema`, `frecuenciaADesarrollar`, `aprenderLenguaje`, `frecuenciaDificultad`, `pagoTrabajo`, `renuncioTrabajo`, `tiempoAdaptacion`, `tiempoTrabajo`, `cursoFormacion`) VALUES
(57, 'Cesar Alcocer Fuentes', 22, 'Masculino', '4563773', 'cesAL@gmail.com', 'Oruro', 'Junior', 'Desarrollo', 'Medio', 'Normal', 'Nunca', 'Nose', 'Casi siempre', 'Nose', 'Si', '6 meses', '6 meses', 'Talves'),
(58, 'Catalina Fuentes Arias', 26, 'Femenino', '75398745', 'cat26@gmail.com', 'Chuquisaca', 'Tecnico', 'Desarrollo', 'Muy alto', 'Normal', 'Casi nunca', 'No', 'A veces', 'Probablemente no', 'No', '6 meses', '3 meses', 'No'),
(59, 'Danitza Carla Almanza Bustamante', 30, 'Femenino', '65489234', 'danitzafor4@gmail.com', 'Cochabamba', 'Ayudante', 'Desarrollo', 'Alto', 'Normal', 'Casi siempre', 'No', 'Casi nunca', 'Probablemente no', 'Si', '6 meses', '6 meses', 'Si'),
(60, 'Tamara Valdivia Anturias', 27, 'Femenino', '4598334', 'tami32_67@gmail.com', 'Cochabamba', 'Junior', 'Base de datos', 'Alto', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Probablemente si', 'Si', '6 meses', '6 meses', 'Si'),
(61, 'Elmer Soria Cordova', 22, 'Masculino', '75203233', 'elmer_soria@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Muy alto', 'Dificl', 'Nunca', 'Si', 'Casi nunca', 'Probablemente si', 'No', '6 meses', '6 meses', 'Talves'),
(62, 'Alberto Hinojosa Cespedes', 27, 'Masculino', '4567291', 'albert_83@hotmail.com', 'Cochabamba', 'Ayudante', 'Redes y telefonia', 'Muy alto', 'Facil', 'Casi siempre', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '3 meses', 'Si'),
(63, 'Juan Carlos Cespedes Ferreira', 27, 'Masculino', '79320099', 'juan@gmail.com', 'Cochabamba', 'Ayudante', 'Redes y telefonia', 'Medio', 'Facil', 'Casi nunca', 'No', 'Nunca', 'Probablemente si', 'No', '1 mes', '3 meses', 'Si'),
(64, 'Jaime Almanza Cordova', 29, 'Masculino', '65883290', 'jaime_89@gmail.com', 'Beni', 'Tecnico', 'HelpDesk', 'Alto', 'Dificl', 'Casi siempre', 'Si', 'A veces', 'Definitivamente no', 'No', '1 mes', '3 meses', 'No'),
(66, 'Carla Rocabado Arias', 25, 'Femenino', '65328798', 'carla_78_90@gmail.com', 'Pando', 'Junior', 'Base de datos', 'Alto', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Probablemente no', 'Si', '3 meses', '3 meses', 'No'),
(69, 'Ruben Carlos Agreda', 27, 'Masculino', '63636344', 'rebuen@gamil.com', 'Lapaz', 'Ayudante', 'Base de datos', 'Muy alto', 'Muy dificil', 'Casi nunca', 'Si', 'A veces', 'Definitivamente si', 'Si', '3 meses', '3 meses', 'Si'),
(70, 'Benito Rioja Flores', 28, 'Masculino', '64723743', 'beni_78-W@gmail.com', 'Lapaz', 'Tecnico', 'Calidad de software', 'Muy alto', 'Dificl', 'Siempre', 'No', 'Nunca', 'Definitivamente si', 'Si', '3 meses', '3 meses', 'No'),
(71, 'Fernanda Vasquez Valdivia', 26, 'Femenino', '65747484', 'fer_16@gmail.com', 'Lapaz', 'Tecnico', 'Redes y telefonia', 'Muy alto', 'Dificl', 'Casi nunca', 'Si', 'Casi nunca', 'Probablemente si', 'Si', '3 meses', '3 meses', 'Si'),
(72, 'Mario Flores Valdivia', 28, 'Masculino', '763421', 'mario_45@gmail.com', 'Santa cruz', 'Semi Senior', 'Desarrollo', 'Alto', 'Dificil', 'Casi Nunca', 'Si', 'A veces', 'Nose', 'No', '6 meses', '6 meses', 'Si'),
(73, 'Carmen Salinas Acha', 24, 'Femenino', '4365789', 'carmen_PP@gmail.com', 'Lapaz', 'Tecnico', 'HelpDesk', 'Muy alto', 'Facil', 'Casi nunca', 'No', 'Nunca', 'Nose', 'Si', '6 meses', '1 mes', 'Si'),
(74, 'Maria Fernanda Arce', 24, 'Femenino', '76542345', 'fernanda_any@gmail.com', 'Lapaz', 'Tecnico', 'Calidad de software', 'Muy alto', 'Muy dificil', 'A veces', 'No', 'A veces', 'Definitivamente si', 'Si', '3 meses', '1 mes', 'No'),
(75, 'Eliana Esteves Diaz', 28, 'Femenino', '4673212', 'eli_an@gmail.com', 'Cochabamba', 'Semi Senior', 'Desarrollo', 'Medio', 'Facil', 'A veces', 'Si', 'Casi nunca', 'Nose', 'No', '1 mes', '1 mes', 'Si'),
(76, 'Carla Martines Cespedes', 25, 'Femenino', '43333322', 'eli_mar@gmail.com', 'Santa cruz', 'Tecnico', 'Calidad de software', 'Muy alto', 'Muy dificil', 'A veces', 'Si', 'Casi nunca', 'Probablemente si', 'No', '9 meses', '1 mes', 'Si'),
(77, 'Fernando Milan Herbas', 27, 'Masculino', '67543298', 'rfernan_65AR@gmail.com', 'Cochabamba', 'Tecnico', 'Desarrollo', 'Alto', 'Dificil', 'A veces', 'Si', 'Nunca', 'Nose', 'Si', '3 meses', '1 mes', 'Si'),
(78, 'Ivan Flores Caceres', 26, 'Masculino', '76574747', 'ivan_9876@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(79, 'Karla Herbas', 27, 'Femenino', '4399833', 'Carnan_65AR@gmail.com', 'Cochabamba', 'Ayudante', 'Desarrollo', 'Alto', 'Dificil', 'A veces', 'Si', 'Nunca', 'Nose', 'Si', '3 meses', '1 mes', 'Si'),
(80, 'Marta Chavarria Caceres', 26, 'Femenino', '76574747', 'ivan@gmail.com', 'Lapaz', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(81, 'Ivana', 22, 'Femenino', '765001147', 'ivana_cer54@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(82, 'Carlos Rocabado Alan', 29, 'Masculino', '76574747', 'carlos_99@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(83, 'Damian Flores Arce', 26, 'Masculino', '721004747', 'damian_87_33@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(84, 'Lorena Garcia Molina', 24, 'Femenino', '76570012', 'Lore_68@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(85, 'Alison Castillo', 23, 'Femenino', '75930744', 'rfernan_65AR@gmail.com', 'Santa cruz', 'Tecnico', 'Desarrollo', 'Alto', 'Dificil', 'A veces', 'Si', 'Nunca', 'Nose', 'Si', '3 meses', '1 mes', 'Si'),
(86, 'Alan Flores Gamarra', 26, 'Masculino', '723009747', 'alan_brito@gmail.com', 'Lapaz', 'Tecnico', 'Base de datos', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(87, 'Juan Carlos Caceres', 26, 'Masculino', '711004747', 'juanca@gmail.com', 'Oruro', 'Tecnico', 'Desarrollo', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(88, 'Charli Rocabado Arias', 30, 'Masculino', '69074747', 'charles_45@gmail.com', 'Cochabamba', 'Tecnico', 'Calidad de software', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(89, 'Carla Cordova', 26, 'Femenino', '76574747', 'ivan@gmail.com', 'Cochabamba', 'Tecnico', 'HelpDesk', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(91, 'Carmen Rojas Salas', 26, 'Femenino', '67824747', 'carmencita@gmail.com', 'Cochabamba', 'Ayudante', 'Base de datos', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(92, 'Mario Rojas Salasar', 26, 'Masculino', '76574747', 'Mario_luis@gmail.com', 'Lapaz', 'Tecnico', 'Calidad de software', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(93, 'Oriana Salas Bermuda', 26, 'Femenino', '76574747', 'ori_fio@gmail.com', 'Lapaz', 'Tecnico', 'Calidad de software', 'Medio', 'Normal', 'Nunca', 'Si', 'A veces', 'Nose', 'Si', '3 meses', '6 meses', 'Si'),
(95, 'Alison Castillo', 23, 'Femenino', '76543456', 'rfernan_65AR@gmail.com', 'Santa cruz', 'Tecnico', 'Desarrollo', 'Alto', 'Dificil', 'A veces', 'Si', 'Nunca', 'Nose', 'Si', '3 meses', '1 mes', 'Si'),
(98, 'Dante Rojas', 26, 'Masculino', '65774747', 'dante_87@gmail.com', 'Lapaz', 'Tecnico', 'Desarrollo', 'Alto', 'Normal', 'Nunca', 'No', 'A veces', 'Definitivamente si', 'Si', '3 meses', '3 meses', 'No'),
(99, 'Norman Alba Cespedes', 27, 'Masculino', '4532234', 'norman_65@gmail.com', 'Sant cruz', 'Tecnico', 'Calidad de software', 'Muy alto', 'Dificl', 'A veces', 'No', 'Casi siempre', 'Probablemente no', '', '3 meses', '6 meses', 'No'),
(100, 'Cesar Martines Rojas', 22, 'Masculino', '4653889', 'alb_89@gmail.com', 'Potosi', 'Junior', 'Redes Sociales', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(101, 'Jhonatan Bazoalto Arce', 25, 'Masculino', '49977663', 'alb_89@gmail.com', 'Cochabamba', 'Semi Senior', 'Redes y telefonia', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(102, 'Jose Julio Argote', 29, 'Masculino', '49977663', 'alb_89@gmail.com', 'Lapaz', 'Tecnico', 'Calidad de software', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(103, 'Carlos Marco Salas', 25, 'Masculino', '4008345', 'carlos_98@gmail.com', 'Tarija', 'Tecnico', 'Redes y telefonia', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(104, 'Wilder Arce Valdivia', 25, 'Masculino', '4008345', 'willy_65ar@gmail.com', 'Sant cruz', 'Tecnico', 'Redes y telefonia', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(105, 'Roberto Cuevas Solano', 25, 'Masculino', '4653837', 'rober_man@gmail.com', 'Cochabamba', 'Tecnico', 'Redes y telefonia', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(106, 'Rodrigo Valdez Rojas', 29, 'Masculino', '4653837', 'rodri_1990@gmail.com', 'Cochabamba', 'Tecnico', 'Redes y telefonia', 'Bajo', 'Dificl', 'Casi nunca', 'No', 'A veces', 'Nose', 'Si', '6 meses', '6 meses', 'No'),
(107, 'Roman Cespedes Alcocer', 29, 'Masculino', '65983444', 'roman_87@gmail.com', 'Cochabamba', 'Semi Senior', 'Redes y telefonia', 'Medio', 'Normal', 'Casi nunca', 'No', 'Casi nunca', 'Probablemente si', 'Si', '3 meses', '3 meses', 'Si'),
(108, 'Roberto Salinas Martines', 29, 'Masculino', '65983444', 'roberto_pes@gmail.com', 'Cochabamba', 'Ayudante', 'Redes y telefonia', 'Medio', 'Normal', 'Casi nunca', 'No', 'Casi nunca', 'Probablemente si', 'Si', '3 meses', '3 meses', 'Si');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bd`
--
ALTER TABLE `bd`
  ADD PRIMARY KEY (`idBd`);

--
-- Indices de la tabla `desarrollo`
--
ALTER TABLE `desarrollo`
  ADD PRIMARY KEY (`idDesarrollo`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idEmpresa`);

--
-- Indices de la tabla `helpdesk`
--
ALTER TABLE `helpdesk`
  ADD PRIMARY KEY (`idHelpDesk`);

--
-- Indices de la tabla `preguntarespuesta`
--
ALTER TABLE `preguntarespuesta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-pregunta_idx` (`idPregunta`),
  ADD KEY `fk-respuesta_idx` (`idRespuesta`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `qa`
--
ALTER TABLE `qa`
  ADD PRIMARY KEY (`idQa`);

--
-- Indices de la tabla `redes`
--
ALTER TABLE `redes`
  ADD PRIMARY KEY (`idRedes`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`idRespuesta`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bd`
--
ALTER TABLE `bd`
  MODIFY `idBd` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `desarrollo`
--
ALTER TABLE `desarrollo`
  MODIFY `idDesarrollo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idEmpresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `helpdesk`
--
ALTER TABLE `helpdesk`
  MODIFY `idHelpDesk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `preguntarespuesta`
--
ALTER TABLE `preguntarespuesta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `qa`
--
ALTER TABLE `qa`
  MODIFY `idQa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `redes`
--
ALTER TABLE `redes`
  MODIFY `idRedes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `idRespuesta` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `preguntarespuesta`
--
ALTER TABLE `preguntarespuesta`
  ADD CONSTRAINT `fk-pregunta` FOREIGN KEY (`idPregunta`) REFERENCES `preguntas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk-respuesta` FOREIGN KEY (`idRespuesta`) REFERENCES `respuestas` (`idRespuesta`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
