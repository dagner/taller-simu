import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import { DatosGraficaService } from '../../services/datos-grafica.service';
import * as pluginAnnotations from 'chartjs-plugin-annotation';

import { PdfMakeWrapper, Txt, Columns, Stack, Table, TextReference, PageReference, Img } from 'pdfmake-wrapper';

declare var $: any;

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css']
})
export class GraficasComponent implements OnInit {

  constructor(private datosGrafica: DatosGraficaService) { }

  ngOnInit(): void {
  }
  public lineChartData: ChartDataSets[] = [
    { data: this.datosGrafica.listaContagios, label: 'Recientes' },
    { data: this.datosGrafica.listaRecuperaciones, label: 'Aceptados' },
    { data: this.datosGrafica.listaFallecimientos, label: 'Rechazados'}
  ];

  public lineChartData2: ChartDataSets[] = [
    { data: this.datosGrafica.listaSusceptible, label: 'Susceptibles' },
    { data: this.datosGrafica.listaInfectados, label: 'Infectados' },
    { data: this.datosGrafica.listaRecuperados, label: 'Recuperados'},
    { data: this.datosGrafica.listaFallecidos, label: 'Fallecidos' }
  ];
  public lineChartLabels: Label[] = this.datosGrafica.dias;
  public lineChartOptions: any = {
    responsive: true
  }
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'transparent',
      borderColor: '#3e95cd',
      pointBackgroundColor: '#3e95cd',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'transparent',
      borderColor: '#8e5ea2',
      pointBackgroundColor: '#8e5ea2',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: 'red',
      pointBackgroundColor: 'red',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: '#3cba9f',
      pointBackgroundColor: '#3cba9f',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];
  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


  async pdf(){
    var canvas = $("#chart").get(0);
    var dataURL = canvas.toDataURL();
    //console.log(dataURL);
    var pdf = new PdfMakeWrapper();
    pdf.images({
      picture1: await new Img(dataURL).build()
    })
    pdf.create().open();
    
  }

}


