import { Component, OnInit } from '@angular/core';
import { DatosGraficaService } from 'src/app/services/datos-grafica.service';

import { PdfMakeWrapper, Txt, Columns, Stack, Table, TextReference, PageReference, Img } from 'pdfmake-wrapper';

declare var $: any;
@Component({
  selector: 'app-cuarentena',
  templateUrl: './cuarentena.component.html',
  styleUrls: ['./cuarentena.component.css']
})
export class CuarentenaComponent implements OnInit {

  cantDias;
  susceptibles;
  infectados;
  recuperados = 0;
  fallecidos = 0;
  contagios = 0;
  recuperaciones = 0;
  fallecimientos = 0;

  // datos
  duracionEnfermedad;
  tasaInteraccion;
  probContagios;
  tasaRecuperacion;
  tasaMortalidad;

  // listas
  listaSusceptible: number[] = [];
  listaInfectados: number[] = [];
  listaRecuperados: number[] = [];
  listaFallecidos: number[] = [];
  listaContagios: number[] = [];
  listaRecuperaciones: number[] = [];
  listaFallecimientos: number[] = [];
  dias: number[] = [];

  // resultados
  mostrar = false;
  totalContagios = 0;
  totalRecuperados = 0;
  totalFallecidos = 0;
  noAfectados = 0;
  maxInfectadosSimultaneos = 0;
  maxContagios = 0;
  maxRecuperaciones = 0;
  maxFallecimientos = 0;
  fecha;
  // lista
  datos = [this.listaSusceptible, this.listaInfectados, this.listaRecuperados, this.listaFallecidos];
  constructor(private datosGrafica: DatosGraficaService) { }

  ngOnInit(): void {
  this.cantDias = $('#cantDias').val();
  console.log(Math.round(99982.845));
  this.fecha = new Date().getDate() + '/' + new Date().getMonth() + '/' + new Date().getFullYear();

  }

  cantidadDias() {
    this.cantDias = $('#cantDias').val();
  }

  simular2() {
    this.listaSusceptible    = [];
    this.listaInfectados     = [];
    this.listaRecuperados    = [];
    this.listaFallecidos     = [];
    this.listaContagios      = [];
    this.listaRecuperaciones = [];
    this.listaFallecimientos = [];
    this.dias                = [];
    var vulnerables          = parseInt($('#susceptibles').val());
    var tasaCuarentena       = parseInt($('#tasaCuarentena').val());
    var susceptibles         = (1-(tasaCuarentena / 100)) * vulnerables;
    var infectados           = parseInt($('#infectados').val());
    var recuperados          = 0;
    var fallecidos           = 0;
    var contagios            = 0;
    var recuperaciones       = 0;
    var fallecimientos       = 0;
    var duracionEnfermedad   = parseInt($('#duracionEnfermedad').val());
    var tasaInteraccion      = parseInt($('#tasaInteraccion').val());
    var probContagios        = (parseInt($('#probContagios').val()));
    var tasaRecuperacion     = parseInt($('#tasaRecuperacion').val());
    var tasaMortalidad       = parseInt($('#tasaMortalidad').val());
    var cantDias             = parseInt($('#cantDias').val());
    console.log(probContagios)
    this.listaSusceptible.push(susceptibles);
    this.listaInfectados.push(infectados);
    this.listaRecuperados.push(recuperados);
    this.listaFallecidos.push(fallecidos);
    this.listaContagios.push(contagios);
    this.listaRecuperaciones.push(recuperaciones);
    this.listaFallecimientos.push(fallecimientos);
    for (let i = 1; i <= cantDias; i++) {
      this.dias.push(i);
      contagios = (this.listaInfectados[i - 1] * (tasaInteraccion) * this.listaSusceptible[i - 1]
        / (this.listaSusceptible[i - 1] + this.listaInfectados[i - 1] + this.listaRecuperados[i - 1]) * (probContagios / 100));
      recuperaciones = (this.listaInfectados[i - 1] * (tasaRecuperacion / 100) / duracionEnfermedad);
      fallecimientos = (this.listaInfectados[i - 1] * (tasaMortalidad / 100) / duracionEnfermedad);
      this.listaContagios.push(contagios);
      this.listaRecuperaciones.push(recuperaciones);
      this.listaFallecimientos.push(fallecimientos);
      susceptibles = this.listaSusceptible[i - 1] - this.listaContagios[i];
      infectados = this.listaInfectados[i - 1] + this.listaContagios[i] - this.listaRecuperaciones[i] - this.listaFallecimientos[i];
      recuperados = this.listaRecuperados[i - 1] + this.listaRecuperaciones[i];
      fallecidos = this.listaFallecidos[i - 1] + this.listaFallecimientos[i];
      this.listaSusceptible.push(susceptibles);
      this.listaInfectados.push(infectados);
      this.listaRecuperados.push(recuperados);
      this.listaFallecidos.push(fallecidos);
    }
    this.resultados();
    this.cargarDatosServicio();
  }

  resultados(){
    this.mostrar = true;
    this.totalContagios = 0;
    this.totalRecuperados = 0;
    this.totalFallecidos = 0;
    this.noAfectados = 0;
    this.maxInfectadosSimultaneos = 0;
    this.maxContagios = 0;
    this.maxRecuperaciones = 0;
    this.maxFallecimientos = 0;
    for (let i = 0; i <= this.cantDias; i++) {
      this.totalContagios += this.listaContagios[i];
      this.totalRecuperados += this.listaRecuperaciones[i];
      this.totalFallecidos += this.listaFallecimientos[i];

    }
    this.totalContagios += this.listaInfectados[0];
    this.noAfectados = this.listaSusceptible[0] + this.listaInfectados[0] - this.totalContagios;
    this.maxInfectadosSimultaneos = Math.max(...this.listaInfectados);
    this.maxContagios = Math.max(...this.listaContagios);
    this.maxRecuperaciones = Math.max(...this.listaRecuperaciones);
    this.maxFallecimientos = Math.max(...this.listaFallecimientos);
  }

  cargarDatosServicio() {
    this.datosGrafica.listaSusceptible = this.listaSusceptible;
    this.datosGrafica.listaInfectados = this.listaInfectados;
    this.datosGrafica.listaRecuperados = this.listaRecuperados;
    this.datosGrafica.listaFallecidos = this.listaFallecidos;
    this.datosGrafica.listaContagios = this.listaContagios;
    this.datosGrafica.listaRecuperaciones = this.listaRecuperaciones;
    this.datosGrafica.listaFallecimientos = this.listaFallecimientos;
    this.datosGrafica.dias = this.dias;
  }

  pdf() {
    var pdf = new PdfMakeWrapper();
    pdf.pageMargins([50, 60]);
    pdf.info({
      title: 'reporte',
      author: 'Taller'
    });
    //marca de agua
    pdf.watermark({ text: 'Universidad Mayor de San Simón', color: '#f2f2f2', opacity: 0.3, bold: true });

    //la cabecera
    pdf.add(new Txt('Reporte epidemiológico virus COVID-19').alignment('center').fontSize(20).bold().end);
    pdf.add(
      pdf.ln(1)
    );
    //el pie de pagina
    pdf.footer(new Txt('- Documento sin validez legal').fontSize(15).color('#f2f2f2').opacity(0.5).alignment('center').end);
    pdf.add(new Txt('Fecha: ' + this.fecha).fontSize(20).bold().alignment('right').end);
    pdf.add(
      pdf.ln(1)
    );
    pdf.add(new Txt('- Datos iniciales de la pandemia COVID-19').fontSize(20).bold().end);
    pdf.add(
      pdf.ln(1)
    );
    pdf.add(
      new Table([
        [{ text: this.capitalize('Personas Susceptibles'), fontSize: 18, bold: true },
        { text: this.listaSusceptible[0], fontSize: 15, bold: false, alignment: 'right' }],
        [{ text: this.capitalize('Personas infectadas'), fontSize: 18, bold: true },
        { text: this.listaInfectados[0], fontSize: 15, bold: false, alignment: 'right' }],
        [{ text: this.capitalize('Personas recuperadas'), fontSize: 18, bold: true },
        { text: '0', fontSize: 15, bold: false, alignment: 'right' }],
        [{ text: this.capitalize('Personas fallecidas'), fontSize: 18, bold: true },
        { text: '0', fontSize: 15, bold: false, alignment: 'right' }]
      ]).widths([ 300, '*' ]).end
    );
    
    //salto de lineas
    pdf.add(
      pdf.ln(3)
    );
    pdf.add(new Txt('- Datos finales pandemia COVID-19:').fontSize(20).bold().end);
    pdf.add(
      pdf.ln(1)
    );
      pdf.add(
        new Table([
          [ {text: 'Poblacion total contagiada', fontSize: 18}, {text: Math.round(this.totalContagios), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Poblacion total recuperada', fontSize: 18}, {text: Math.round(this.totalRecuperados), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Poblacion total fallecida', fontSize: 18}, {text: Math.round(this.totalFallecidos), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Poblacion no afectada', fontSize: 18}, {text: Math.round(this.noAfectados), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Max. Contagios diarios', fontSize: 18}, {text: Math.round(this.maxContagios), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Max. Recuperaciones diarias', fontSize: 18}, {text: Math.round(this.maxRecuperaciones), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Max. fallecimientos', fontSize: 18}, {text: Math.round(this.maxFallecimientos), bold: false, alignment: 'right', fontSize: 15}],
          [ {text: 'Max. Infectados Simulataneos', fontSize: 18}, {text: Math.round(this.maxInfectadosSimultaneos), bold: false, alignment: 'right', fontSize: 15}]

      ]).widths([ 300, '*' ]).end
      )

    pdf.defaultStyle({
      bold: true,
      fontSize: 20
    });
    pdf.create().open()
  }

  capitalize(word) {
    return word[0].toUpperCase() + word.slice(1);
  }

}
