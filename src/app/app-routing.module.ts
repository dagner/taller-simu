import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatosComponent } from './pages/datos/datos.component';
import { GraficasComponent } from './pages/graficas/graficas.component';
import { HomeComponent } from './pages/home/home.component';
import { CuarentenaComponent } from './pages/cuarentena/cuarentena.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ListaComponent } from './lista/lista.component';
import { ContentComponent } from './content/content.component';
import { PanelComponent } from './panel/panel.component';
import { ListaGraficasComponent } from './lista-graficas/lista-graficas.component';

const routes: Routes = [
  { path: 'datos', component: DatosComponent },
  { path: 'home', component: HomeComponent },
  { path: 'graficas', component: GraficasComponent },
  { path: 'cuarentena', component: CuarentenaComponent },
  { path: 'formulario', component: FormularioComponent },
  { path: 'lista', component: ListaComponent },
  { path: 'content', component: ContentComponent },
  { path:'panel', component: PanelComponent },
  { path:'lista-graficas', component: ListaGraficasComponent },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
