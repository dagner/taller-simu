import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbasededatosComponent } from './fbasededatos.component';

describe('FbasededatosComponent', () => {
  let component: FbasededatosComponent;
  let fixture: ComponentFixture<FbasededatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbasededatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbasededatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
